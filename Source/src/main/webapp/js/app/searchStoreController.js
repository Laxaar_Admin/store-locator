/**
 * <pre>
 * API Type: Angular Controller,
 * Module: storeModule,
 * Description :
 * This is API used as controller for controlling the index.html views.
 * </pre>
 */
(function() {
	/* Start: Controller Configuration */
	var moduleName = "storeModule";
	var module = angular.module(moduleName);
	var controllerName = "SearchStoreController";
	var injectedDependencies = [ '$scope', '$timeout', '$filter', '$rootScope', 'storeService' ];
	/* End: Controller Configuration */

	/* Start:Controller Definition */
	var controller = function($scope, $timeout, $filter, $rootScope, storeService) {

		/*
		 * Search request success callback
		 */
		var searchSuccess = function(stores) {
			$scope.stores = stores;
		};
		/*
		 * Search request Fail callback
		 */
		var searchFail = function(data) {
			$scope.searchError = data.message;
		};

		/**
		 * Search stores using zipCode in scope
		 */
		var searchStores = function() {
			$scope.searchError = null;
			if ($scope.zipCode) {
				storeService.searchStores($scope.zipCode, searchSuccess, searchFail);
			}
		};

		/*
		 * This code ensures that angular template is shown only after it is
		 * rendered
		 */
		$timeout(function() {
			$rootScope.applicationLoaded = true;
		}, 500);
		/* Expose the Functions via scope */
		$scope.searchStores = searchStores;
		$rootScope.$on('FETCH_SEARCH_RESULTS',searchStores);
	};
	/* End:Controller Definition */

	/* Register the controller */
	controller.$inject = injectedDependencies;
	module.controller(controllerName, controller);
}());
