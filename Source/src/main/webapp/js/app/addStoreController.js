/**
 * <pre>
 * API Type: Angular Controller,
 * Module: storeModule,
 * Description :
 * This is API used as controller for controlling the index.html views.
 * </pre>
 */
(function() {
	/* Start: Controller Configuration */
	var moduleName = "storeModule";
	var module = angular.module(moduleName);
	var controllerName = "AddStoreController";
	var injectedDependencies = [ '$scope', '$timeout', '$rootScope', 'storeService' ];
	/* End: Controller Configuration */

	/* Start:Controller Definition */
	var controller = function($scope, $timeout, $rootScope, storeService) {

		var getNewStore = function() {
			$scope.newStore = storeService.getNewStore();
		};
		/*
		 * Save request success callback
		 */
		var saveSuccess = function(store) {

			$scope.saveSuccess = true;
			$rootScope.$broadcast('FETCH_SEARCH_RESULTS');
			$timeout(function() {
				$("#store-add").modal("hide");
				getNewStore();
				$scope.saveError = null;
				$scope.saveSuccess = false;
				$scope.storeForm.$setPristine();
			}, 500)
		};
		/*
		 * Save request Fail callback
		 */
		var saveFail = function(error) {
			$scope.saveError = " while saving data ::" + error ? error.message : 'Unknown Error';
		};
	
		
		/**
		 * Save the store in scope via store service
		 * 
		 */
		var saveStore = function() {
			if ($scope.newStore) {
				storeService.saveStore($scope.newStore, saveSuccess, saveFail);
				
			}
		};
		/**
		 * Search stores using zipCode in scope
		 */
		var searchStores = function() {
			$scope.searchError = null;
			if ($scope.zipCode) {
				storeService.searchStores($scope.zipCode, searchSuccess, searchFail);
			}
		};
		
		/*
		 * This code ensures that angular template
		 * is shown only after it is rendered
		 * */
		$timeout(function() {
			$scope.applicationLoaded = true;
		}, 500);
		$scope.saveStore = saveStore;
		

	};
	/* End:Controller Definition */

	/* Register the controller */
	controller.$inject = injectedDependencies;
	module.controller(controllerName, controller);
}());
