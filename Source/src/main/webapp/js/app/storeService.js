/**
 * <pre>
 * API Type:Angular Service,
 * Module: storeModule,
 * Description :
 * This is API used for calling Rest API's to fetch and save stores.
 * </pre>
 */
(function() {
	/* Start:Service Configuration */
	var moduleName = "storeModule";
	var module = angular.module(moduleName);
	var serviceName = "storeService";
	var injectedDependencies = [ '$http' ];
	/* End: Configuration */

	/* Start:Service Definition */
	var service = function($http) {
		/**
		 * Save a store to server
		 * @param store- store object
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 * */
		var saveStore = function(store, successCallback, failCallback) {
			var success = function(data) {
				if (successCallback) {
					successCallback(data);
				}
			};
			var fail = function(response) {
				if (failCallback) {
					failCallback(response.data,response);
				}
			}
			$http.post("/store", store).then(success, fail);
		};
		/**
		 * Search Stores via zip code
		 * @param zipCode of the store
		 * @param successCallback- Success callback to be executed
		 * @param failCallback- Fail callback to be executed
		 * */
		var searchStores = function(zipCode, successCallback, failCallback) {
			var success = function(response) {
				if (successCallback) {
					successCallback(response.data,response);
				}
			};
			var fail = function(response) {
				
				if (failCallback) {
					failCallback(response.data,response);
				}
			}
			$http.get("/store/"+zipCode).then(success, fail);
		};
		/**
		 * Get a new store instance
		 * @returns New Store instance
		 * */
		var getNewStore=function(){
			return {
				storeId:null,
				storeLocation:null,				
				storeName:null,
				storeDescription:null,
				zipCode:null,
			}
		};
		return {
			'saveStore' : saveStore,
			'searchStores':searchStores,
			'getNewStore':getNewStore
		};
	};
	/* End:Service Declaration */

	/* Start:Service Registration */
	service.$inject = injectedDependencies;
	module.factory(serviceName, service);
	/* End:Service Registration */
}());