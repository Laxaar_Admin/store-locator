/**
 * <pre>
 * API Type: Angular Module,
 * Description :
 * This is an angular Module used bootstrap the application by angular. 
 * It has associated controller and service which deals with data management on the views
 * </pre>
 */
(function() {
	/* Start:Module Configuration */
	var moduleName = "storeModule";
	// Module Dependencies
	var injectedDependencies = [ 'angularUtils.directives.dirPagination' ];
	/* End Configuration */

	// Register Module
	angular.module(moduleName, injectedDependencies);
}());
