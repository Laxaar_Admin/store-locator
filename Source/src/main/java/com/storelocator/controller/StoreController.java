package com.storelocator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.storelocator.entity.Store;
import com.storelocator.service.StoreService;

/**
 * @author Inderdeep Singh
 * Controller class to expose the Store Rest API
 *
 */
@RestController
@RequestMapping("/store")
public class StoreController {

	@Autowired
	private StoreService storeService;

	/**
	 * Get the nearby stores based on zipcode
	 * @param zipCode
	 * @return List of stores nearby 
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "{zipCode}")
	@ResponseBody
	public List<Store> getStores(@PathVariable String zipCode) throws Exception {
	
		if (zipCode != null && !zipCode.isEmpty()) {
			
			return storeService.findByZipCode(Long.valueOf(zipCode));
		}
		return null;
	}

	/**
	 * Save a store
	 * @param store
	 * @return Saved instance of store
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Store saveStore(@RequestBody Store store) throws Exception {
		if (store != null) {
		
			return storeService.saveStore(store);
		}
		else{
			return null;
		}
	}
}
